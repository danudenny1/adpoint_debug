<?php
namespace Magento\Catalog\Model\ResourceModel\Category\Flat;

/**
 * Interceptor class for @see \Magento\Catalog\Model\ResourceModel\Category\Flat
 */
class Interceptor extends \Magento\Catalog\Model\ResourceModel\Category\Flat implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context, \Magento\Framework\Indexer\Table\StrategyInterface $tableStrategy, \Magento\Catalog\Model\CategoryFactory $categoryFactory, \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Catalog\Model\Config $catalogConfig, \Magento\Framework\Event\ManagerInterface $eventManager, $connectionName = null, \Magento\Catalog\Model\ResourceModel\Category\Flat\CollectionFactory $categoryFlatCollectionFactory = null)
    {
        $this->___init();
        parent::__construct($context, $tableStrategy, $categoryFactory, $categoryCollectionFactory, $storeManager, $catalogConfig, $eventManager, $connectionName, $categoryFlatCollectionFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function setStoreId($storeId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setStoreId');
        if (!$pluginInfo) {
            return parent::setStoreId($storeId);
        } else {
            return $this->___callPlugins('setStoreId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getStoreId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getStoreId');
        if (!$pluginInfo) {
            return parent::getStoreId();
        } else {
            return $this->___callPlugins('getStoreId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMainTable()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMainTable');
        if (!$pluginInfo) {
            return parent::getMainTable();
        } else {
            return $this->___callPlugins('getMainTable', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMainStoreTable($storeId = 0)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMainStoreTable');
        if (!$pluginInfo) {
            return parent::getMainStoreTable($storeId);
        } else {
            return $this->___callPlugins('getMainStoreTable', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addInactiveCategoryIds($ids)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'addInactiveCategoryIds');
        if (!$pluginInfo) {
            return parent::addInactiveCategoryIds($ids);
        } else {
            return $this->___callPlugins('addInactiveCategoryIds', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getInactiveCategoryIds()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getInactiveCategoryIds');
        if (!$pluginInfo) {
            return parent::getInactiveCategoryIds();
        } else {
            return $this->___callPlugins('getInactiveCategoryIds', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addChildNodes($children, $path, $parent)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'addChildNodes');
        if (!$pluginInfo) {
            return parent::addChildNodes($children, $path, $parent);
        } else {
            return $this->___callPlugins('addChildNodes', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getNodes($parentId, $recursionLevel = 0, $storeId = 0)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getNodes');
        if (!$pluginInfo) {
            return parent::getNodes($parentId, $recursionLevel, $storeId);
        } else {
            return $this->___callPlugins('getNodes', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCategories($parent, $recursionLevel = 0, $sorted = false, $asCollection = false, $toLoad = true)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCategories');
        if (!$pluginInfo) {
            return parent::getCategories($parent, $recursionLevel, $sorted, $asCollection, $toLoad);
        } else {
            return $this->___callPlugins('getCategories', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getNodeById($nodeId, $nodes = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getNodeById');
        if (!$pluginInfo) {
            return parent::getNodeById($nodeId, $nodes);
        } else {
            return $this->___callPlugins('getNodeById', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAttribute($attribute)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAttribute');
        if (!$pluginInfo) {
            return parent::getAttribute($attribute);
        } else {
            return $this->___callPlugins('getAttribute', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getChildrenAmount($category, $isActiveFlag = true)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getChildrenAmount');
        if (!$pluginInfo) {
            return parent::getChildrenAmount($category, $isActiveFlag);
        } else {
            return $this->___callPlugins('getChildrenAmount', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProductCount($category)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProductCount');
        if (!$pluginInfo) {
            return parent::getProductCount($category);
        } else {
            return $this->___callPlugins('getProductCount', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getParentCategories($category, $isActive = true)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getParentCategories');
        if (!$pluginInfo) {
            return parent::getParentCategories($category, $isActive);
        } else {
            return $this->___callPlugins('getParentCategories', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getParentDesignCategory($category)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getParentDesignCategory');
        if (!$pluginInfo) {
            return parent::getParentDesignCategory($category);
        } else {
            return $this->___callPlugins('getParentDesignCategory', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getChildrenCategories($category)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getChildrenCategories');
        if (!$pluginInfo) {
            return parent::getChildrenCategories($category);
        } else {
            return $this->___callPlugins('getChildrenCategories', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isInRootCategoryList($category)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isInRootCategoryList');
        if (!$pluginInfo) {
            return parent::isInRootCategoryList($category);
        } else {
            return $this->___callPlugins('isInRootCategoryList', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getChildren($category, $recursive = true, $isActive = true)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getChildren');
        if (!$pluginInfo) {
            return parent::getChildren($category, $recursive, $isActive);
        } else {
            return $this->___callPlugins('getChildren', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAllChildren($category)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAllChildren');
        if (!$pluginInfo) {
            return parent::getAllChildren($category);
        } else {
            return $this->___callPlugins('getAllChildren', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function checkId($id)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'checkId');
        if (!$pluginInfo) {
            return parent::checkId($id);
        } else {
            return $this->___callPlugins('checkId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAnchorsAbove(array $filterIds, $storeId = 0)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAnchorsAbove');
        if (!$pluginInfo) {
            return parent::getAnchorsAbove($filterIds, $storeId);
        } else {
            return $this->___callPlugins('getAnchorsAbove', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProductsPosition($category)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProductsPosition');
        if (!$pluginInfo) {
            return parent::getProductsPosition($category);
        } else {
            return $this->___callPlugins('getProductsPosition', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function reindexAll()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'reindexAll');
        if (!$pluginInfo) {
            return parent::reindexAll();
        } else {
            return $this->___callPlugins('reindexAll', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getIdxTable($table = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getIdxTable');
        if (!$pluginInfo) {
            return parent::getIdxTable($table);
        } else {
            return $this->___callPlugins('getIdxTable', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function syncData()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'syncData');
        if (!$pluginInfo) {
            return parent::syncData();
        } else {
            return $this->___callPlugins('syncData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function insertFromTable($sourceTable, $destTable, $readToIndex = true)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'insertFromTable');
        if (!$pluginInfo) {
            return parent::insertFromTable($sourceTable, $destTable, $readToIndex);
        } else {
            return $this->___callPlugins('insertFromTable', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function insertFromSelect($select, $destTable, array $columns, $readToIndex = true)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'insertFromSelect');
        if (!$pluginInfo) {
            return parent::insertFromSelect($select, $destTable, $columns, $readToIndex);
        } else {
            return $this->___callPlugins('insertFromSelect', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function clearTemporaryIndexTable()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'clearTemporaryIndexTable');
        if (!$pluginInfo) {
            return parent::clearTemporaryIndexTable();
        } else {
            return $this->___callPlugins('clearTemporaryIndexTable', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getIdFieldName()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getIdFieldName');
        if (!$pluginInfo) {
            return parent::getIdFieldName();
        } else {
            return $this->___callPlugins('getIdFieldName', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTable($tableName)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getTable');
        if (!$pluginInfo) {
            return parent::getTable($tableName);
        } else {
            return $this->___callPlugins('getTable', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getConnection()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getConnection');
        if (!$pluginInfo) {
            return parent::getConnection();
        } else {
            return $this->___callPlugins('getConnection', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'load');
        if (!$pluginInfo) {
            return parent::load($object, $value, $field);
        } else {
            return $this->___callPlugins('load', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Magento\Framework\Model\AbstractModel $object)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'save');
        if (!$pluginInfo) {
            return parent::save($object);
        } else {
            return $this->___callPlugins('save', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function delete(\Magento\Framework\Model\AbstractModel $object)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'delete');
        if (!$pluginInfo) {
            return parent::delete($object);
        } else {
            return $this->___callPlugins('delete', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addUniqueField($field)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'addUniqueField');
        if (!$pluginInfo) {
            return parent::addUniqueField($field);
        } else {
            return $this->___callPlugins('addUniqueField', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function resetUniqueField()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'resetUniqueField');
        if (!$pluginInfo) {
            return parent::resetUniqueField();
        } else {
            return $this->___callPlugins('resetUniqueField', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function unserializeFields(\Magento\Framework\Model\AbstractModel $object)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'unserializeFields');
        if (!$pluginInfo) {
            return parent::unserializeFields($object);
        } else {
            return $this->___callPlugins('unserializeFields', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getUniqueFields()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getUniqueFields');
        if (!$pluginInfo) {
            return parent::getUniqueFields();
        } else {
            return $this->___callPlugins('getUniqueFields', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasDataChanged($object)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'hasDataChanged');
        if (!$pluginInfo) {
            return parent::hasDataChanged($object);
        } else {
            return $this->___callPlugins('hasDataChanged', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getChecksum($table)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getChecksum');
        if (!$pluginInfo) {
            return parent::getChecksum($table);
        } else {
            return $this->___callPlugins('getChecksum', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function afterLoad(\Magento\Framework\DataObject $object)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'afterLoad');
        if (!$pluginInfo) {
            return parent::afterLoad($object);
        } else {
            return $this->___callPlugins('afterLoad', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave(\Magento\Framework\DataObject $object)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'beforeSave');
        if (!$pluginInfo) {
            return parent::beforeSave($object);
        } else {
            return $this->___callPlugins('beforeSave', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave(\Magento\Framework\DataObject $object)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'afterSave');
        if (!$pluginInfo) {
            return parent::afterSave($object);
        } else {
            return $this->___callPlugins('afterSave', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function beforeDelete(\Magento\Framework\DataObject $object)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'beforeDelete');
        if (!$pluginInfo) {
            return parent::beforeDelete($object);
        } else {
            return $this->___callPlugins('beforeDelete', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function afterDelete(\Magento\Framework\DataObject $object)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'afterDelete');
        if (!$pluginInfo) {
            return parent::afterDelete($object);
        } else {
            return $this->___callPlugins('afterDelete', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function serializeFields(\Magento\Framework\Model\AbstractModel $object)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'serializeFields');
        if (!$pluginInfo) {
            return parent::serializeFields($object);
        } else {
            return $this->___callPlugins('serializeFields', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function beginTransaction()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'beginTransaction');
        if (!$pluginInfo) {
            return parent::beginTransaction();
        } else {
            return $this->___callPlugins('beginTransaction', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addCommitCallback($callback)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'addCommitCallback');
        if (!$pluginInfo) {
            return parent::addCommitCallback($callback);
        } else {
            return $this->___callPlugins('addCommitCallback', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function commit()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'commit');
        if (!$pluginInfo) {
            return parent::commit();
        } else {
            return $this->___callPlugins('commit', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rollBack()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'rollBack');
        if (!$pluginInfo) {
            return parent::rollBack();
        } else {
            return $this->___callPlugins('rollBack', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getValidationRulesBeforeSave()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getValidationRulesBeforeSave');
        if (!$pluginInfo) {
            return parent::getValidationRulesBeforeSave();
        } else {
            return $this->___callPlugins('getValidationRulesBeforeSave', func_get_args(), $pluginInfo);
        }
    }
}
